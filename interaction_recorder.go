package recorder

import (
	"net/http"
)

type Listener interface {
	RequestReceived(request Request, response Response)
}

func Middleware(listeners ...Listener) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return &recorder{next, listeners}
	}
}

func Handler(next http.Handler, listeners ...Listener) http.Handler {
	return Middleware(listeners...)(next)
}

type recorder struct {
	originalHandler http.Handler
	listeners       []Listener
}

func (interceptor *recorder) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	request := createRequest(r)
	responseWrapper := WrapResponseWriter(w)
	interceptor.originalHandler.ServeHTTP(responseWrapper, r)
	response := createResponse(responseWrapper)

	for _, listener := range interceptor.listeners {
		listener.RequestReceived(request, response)
	}
}
