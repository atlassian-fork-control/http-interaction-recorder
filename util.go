package recorder

func copyValues(original map[string][]string) map[string][]string {
	newMap := make(map[string][]string, len(original))
	for key, values := range original {
		newValues := make([]string, len(values))
		copy(newValues, values)
		newMap[key] = newValues
	}

	return newMap
}
