package mocks

import "github.com/stretchr/testify/mock"

type MockIDGenerator struct {
	mock.Mock
}

func (generator *MockIDGenerator) Generate() string {
	args := generator.Called()
	return args.String(0)
}

func (generator *MockIDGenerator) GivenGeneratorReturnsID(id string) {
	generator.On("Generate").Return(id)
}

func (generator *MockIDGenerator) Reset() {
	generator.ExpectedCalls = []*mock.Call{}
}

func NewMockIDGenerator() *MockIDGenerator {
	return new(MockIDGenerator)
}
