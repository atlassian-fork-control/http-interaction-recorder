package mocks

import (
	"encoding/json"

	"bitbucket.org/atlassian/http-interaction-recorder/listeners/pact"
)

type MockWriteCloser struct {
	wasClosed    bool
	bytesWritten []byte
	writeError   error
	closeError   error
}

func (writer *MockWriteCloser) Write(bytes []byte) (int, error) {
	writer.bytesWritten = bytes
	return len(bytes), writer.writeError
}

func (writer *MockWriteCloser) Close() error {
	writer.wasClosed = true
	return writer.closeError
}

func (writer *MockWriteCloser) GivenWriteFailsWith(err error) {
	writer.writeError = err
}

func (writer *MockWriteCloser) GivenCloseFailsWith(err error) {
	writer.closeError = err
}

func (writer *MockWriteCloser) GetWrittenContent() string {
	return string(writer.bytesWritten)
}

func (writer *MockWriteCloser) WasClosed() bool {
	return writer.wasClosed
}

func (writer *MockWriteCloser) GetWrittenPact() pact.Pact {
	var writtenPact pact.Pact
	err := json.Unmarshal(writer.bytesWritten, &writtenPact)
	if err != nil {
		panic(err)
	}
	return writtenPact
}

func NewMockWriteCloser() *MockWriteCloser {
	return new(MockWriteCloser)
}
