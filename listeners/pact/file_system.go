package pact

import (
	"io"
	"os"
)

type FileSystem interface {
	PathExists(path string) (bool, error)
	CreatePath(path string) error
	OpenWriteStream(fileName string) (io.WriteCloser, error)
}

type LocalFileSystem struct{}

func (fileSystem *LocalFileSystem) PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}

	return false, err
}

func (fileSystem *LocalFileSystem) CreatePath(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}

func (fileSystem *LocalFileSystem) OpenWriteStream(fileName string) (io.WriteCloser, error) {
	writer, err := os.Create(fileName)
	if err != nil {
		return nil, err
	}

	return writer, nil
}
