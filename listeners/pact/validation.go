package pact

import (
	"fmt"
	"strings"
)

func notNullNorBlank(value, valueName string) string {
	if strings.TrimSpace(value) == "" {
		panic(fmt.Sprintf("%v cannot be empty", valueName))
	}

	return value
}
