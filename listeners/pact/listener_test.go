package pact_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"encoding/json"
	"math/rand"

	"path/filepath"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/listeners/pact"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xeipuuv/gojsonschema"
)

func whenRequestIsSent(listener recorder.Listener) {
	server := httptest.NewServer(newRecorderHandler(listener))
	http.Get(server.URL)
}

func newRecorderHandler(listener recorder.Listener) http.Handler {
	defaultHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`"test"`))
	})

	return recorder.Handler(defaultHandler, listener)
}

func TestMain(m *testing.M) {
	os.RemoveAll("./pacts")
	os.Exit(m.Run())
}

func uniqueName(name string) string {
	return fmt.Sprintf("%v-%v", name, rand.Int())
}

func loadPact(location string) (pact.Pact, error) {
	file, err := os.Open(location)
	if err != nil {
		return pact.Pact{}, err
	}

	var generatedPact pact.Pact
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&generatedPact)

	return generatedPact, err
}

func schemaLocation() string {
	workingDirectory, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	location := filepath.Join(workingDirectory, "../../node_modules/pact-json-schema/schemas/v2/schema.json")
	return fmt.Sprintf("file://%v", location)
}

func pactLocation(relativePactLocation string) string {
	workingDirectory, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	location := filepath.Join(workingDirectory, relativePactLocation)
	return fmt.Sprintf("file://%v", location)
}

func validatePactAgainstJSONSchema(relativePactLocation string, t *testing.T) error {
	schemaLoader := gojsonschema.NewReferenceLoader(schemaLocation())
	pactLoader := gojsonschema.NewReferenceLoader(pactLocation(relativePactLocation))
	result, err := gojsonschema.Validate(schemaLoader, pactLoader)
	if err != nil {
		return nil
	}

	validationErrors := result.Errors()
	if len(validationErrors) > 0 {
		for _, validationError := range validationErrors {
			t.Log(validationError.String())
		}
		return fmt.Errorf("failed to validate pact file against pact schema")
	}
	return err
}

func TestPactListener(t *testing.T) {
	t.Run("it should consolidate interactions into one file across multiple listener instances", func(t *testing.T) {
		consumer := uniqueName("consumer")
		provider := uniqueName("provider")
		listener1 := pact.NewListener(consumer, provider)
		listener2 := pact.NewListener(consumer, provider)

		whenRequestIsSent(listener1)
		whenRequestIsSent(listener2)

		_, err := os.Stat("./pacts")
		require.NoError(t, err)
		assert.Equal(t, listener1.(pact.Listener).GetPactLocation(), listener2.(pact.Listener).GetPactLocation())
	})

	t.Run("it should write all information into the file", func(t *testing.T) {
		listener := pact.NewListener(uniqueName("consumer"), uniqueName("provider"))
		mockProviderHandler := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			writer.WriteHeader(201)
			writer.Header().Add("custom-response-header", "response-header-value")
			writer.Write([]byte(`"response body"`))
		})
		mockServer := httptest.NewServer(recorder.Handler(mockProviderHandler, listener))

		postRequest, err := http.NewRequest("POST", fmt.Sprintf("%v/some/path?query=value", mockServer.URL), bytes.NewBuffer([]byte(`"request body"`)))
		require.NoError(t, err)
		postRequest.Header.Add("content-type", "application/json")
		postRequest.Header.Add("custom-request-header", "request-header-value")
		http.DefaultClient.Do(postRequest)

		generatedPact, err := loadPact(listener.(pact.Listener).GetPactLocation())
		require.NoError(t, err)
		interaction := generatedPact.Interactions[0]
		assert.Equal(t, "POST /some/path -> 201", interaction.Description, "interaction.description")
		assert.Equal(t, "POST", interaction.Request.Method, "interaction.request.method")
		assert.Equal(t, "/some/path", interaction.Request.Path, "interaction.request.path")
		assert.Equal(t, "query=value", interaction.Request.Query, "interaction.request.query")
		assert.Equal(t, `"request body"`, string(interaction.Request.Body), "interaction.request.body")
		assert.Equal(t, "request-header-value", interaction.Request.Headers["custom-request-header"], "interaction.request.header")
		assert.Equal(t, "application/json", interaction.Request.Headers["content-type"], "interaction.request.headers.content-type")
		assert.Equal(t, 201, interaction.Response.Status, "interaction.response.status")
		assert.Equal(t, `"response body"`, string(interaction.Response.Body), "interaction.response.body")
		assert.Equal(t, "response-header-value", interaction.Response.Headers["custom-response-header"], "interaction.response.header")
	})

	t.Run("it should generate a valid pact file according to json schema", func(t *testing.T) {
		listener := pact.NewListener(uniqueName("consumer"), uniqueName("provider"))

		whenRequestIsSent(listener)

		relativePactLocation := listener.(pact.Listener).GetPactLocation()
		err := validatePactAgainstJSONSchema(relativePactLocation, t)
		assert.NoError(t, err)
	})
}
